#include <iostream>
using namespace std;

#include "m_reasignacion.h"

m_reasignacion::m_reasignacion(){}

// Función para resolver colisión con reasignación lineal
int m_reasignacion::r_prueba_Lineal_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    // Sumar 1 cada vez
    hk++;

    // Avance circular por el arreglo
    if (hk == tamano){
        hk = 0;
    }

    // -1 indica vacio
    if (arreglo[hk] == -1){
        return hk;
    
    // Recursiva
    }else {
        r_prueba_Lineal_adicion(arreglo,tamano,dato,hk,hk_original);
    }
}

void m_reasignacion::r_prueba_Lineal_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    // Sumar 1 cada vez
    hk++;

    // Avance circular por el arreglo
    if (hk == tamano){
        hk = 0;
    }

    // Se completo un avance circular
    if (hk == hk_original) {
        cout << "Valor no encontrado en el arreglo" << endl << endl;
    } 

    // Se encontro el dato
    if (arreglo[hk] == dato) {
        cout << "Colisión: " << endl;
        cout << "Dato encontrado ----> " << arreglo[hk] << endl;
        cout << "Posición del dato --> " << hk << endl << endl;

    // recursividad
    }else {
        r_prueba_Lineal_busqueda(arreglo,tamano,dato,hk,hk_original);
    }

}

// Función para resolver colisión con reasignación cuadrática
int m_reasignacion::r_prueba_Cuadratica_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original, int I) {
    // Formula de reasignación cuadratica
    I++;
    hk = hk_original + (I*I);

    // No superar el tamaño del arreglo
    if (!(hk < tamano)) {
        I = 0;
        hk_original++;
        if (hk_original == tamano){
            hk = 0;
        }
        hk = hk_original;
    }

    // -1 significa vacio
    if (arreglo[hk] == -1){
        return hk;
    
    // recursividad
    } else {
        r_prueba_Cuadratica_adicion(arreglo,20,dato,hk,hk_original,I);
    }
}

void m_reasignacion::r_prueba_Cuadratica_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original, int I) {
    // formula de solucion por reasignación cuadratica
    hk = hk_original + (I*I);

    // repetir buscando el dato
    while (arreglo[hk] != -1 && arreglo[hk] != dato) { 
        I++;
        hk = hk_original + (I*I);

        if (hk >= tamano) {
            I = 0;
            hk_original = 1;
            hk = 1;
        }
    }

    // dato no encontrado
    if (arreglo[hk] == -1){
        cout << "Valor no encontrado" << endl << endl;
    
    // dato encontrado
    } else {
        cout << "Colisión: " << endl;
        cout << "Dato encontrado ----> " << arreglo[hk] << endl;
        cout << "Posición del dato --> " << hk << endl << endl;
    }
}

// Función para resolver colisión con reasignación de doble dirección hash
int m_reasignacion::r_Doble_direc_hash_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    //repetir la funcion hash
    hk = (hk_original % 19);

    // insercion -1 significa vacio
    if (arreglo[hk] == -1) {
        return hk;
    
    // recursividad
    }else {
        r_Doble_direc_hash_adicion(arreglo,20,dato,hk,hk+1);
    }
}

void m_reasignacion::r_Doble_direc_hash_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    // repetir funcion hash
    hk = (hk_original % 19);

    // ciclo de busqueda del dato
    while((hk < tamano) && (arreglo[hk] != -1) && (arreglo[hk] != dato) && (hk != hk_original)) {
        hk = ((hk + 1) % 19);
    }

    // si no encontrado
    if ((arreglo[hk] == -1) || (arreglo[hk] != dato)) {
        cout << "Valor no encontrado" << endl << endl;
    
    // entonces encontrado
    }else {
        cout << "Colisión: " << endl;
        cout << "Dato encontrado ----> " << arreglo[hk] << endl;
        cout << "Posición del dato --> " << hk << endl << endl;
    }
}

// Función para resolver colisión con encadenamiento
void m_reasignacion::Encadenamiento_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    // crear nodo
    Nodo *conjunto = new Nodo();

    // si es null entonces insertar
    if (conjunto == NULL) {
        conjunto->dato = arreglo[hk_original];

    }

    // entonces repetir mientras buscar vacio
    while ((conjunto != NULL ) && conjunto->dato != dato) {
        conjunto = conjunto->next;
    }

    // si encontrar vacio entonces insertar
    if (conjunto == NULL) {
        conjunto->dato = arreglo[hk_original];

    }
}

void m_reasignacion::Encadenamiento_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original) {
    // crear nodo
    Nodo *conjunto = new Nodo();

    // repetir mientras busca dato
    while ((conjunto != NULL ) && conjunto->dato != dato) {
        conjunto = conjunto->next;
    }

    // si es null entonces no se encontro
    if (conjunto != NULL) {
        cout << "El valor no se encuentra en la lista" << endl << endl;

    // entonces si se encontro
    } else {
        cout << "El valor si se encuentra en la lista de la posición: " << hk << endl << endl;
    }
}