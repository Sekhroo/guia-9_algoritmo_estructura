#include <iostream>
using namespace std;

#include "m_reasignacion.h"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt, int indice) {
    cout << "============ Menu ===========" << endl;
    cout << "Añadir datos ("<<indice<<"/20) ____ [1]" << endl;
    cout << "Buscar un dato __________ [2]" << endl;
    cout << "Mostrar el arreglo ______ [3]" << endl;
    cout << "Salir del programa ______ [0]" << endl;
    cout << "=============================" << endl;
    cout << "Opción: ";

    cin >> opt;

    clear();

    return opt;
}

/* Función para el preguntar dato al usuario */
int solicitar_dato (int dato, string enunciado) {
    cout << "======== " << enunciado << " ========" << endl;
    cout << "Ingresar: ";
    cin >> dato;

    clear();

    return dato;
}


/* Función main */
int main(int argc, char **argv){

    // Valida cantidad de parámetros mínimos.
    if (argc<2) {
		cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl;
		cout << "-> Ejemplo: ./hash {L|C|D|E}" << endl;

        return 0;
    }

    // String que indica que método de reasignación utilizar.
    string metodo = argv[1];
    
    // Valida que argv[1] sea una mayuscula
    if (!(metodo == "L") && !(metodo == "C") && !(metodo == "D") && !(metodo == "E")) {
        cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl;
		cout << "-> Ejemplo: ./hash {L|C|D|E}" << endl;
    
        return 0;
    }

    // Inicializa las variables para el menu
    string option = "\0";
    int indice = 0;
    int dato = 0;

    // Métodos de reasignacion (clase)
    m_reasignacion *solve_colision = new m_reasignacion();

    // Crear lista enlazada en el caso de utilizar E
    if (metodo == "E") {
        Nodo *conjunto = new Nodo();
    }

    // Inicializa un Array del tipo int
    // se rellena con -1 para verificar posteriormente una colisión
    int arreglo[20];
    for (int i=0;i<20;i++){
        arreglo[i] = -1;
    }

    clear();

    // Formula hash: h(k) = (k % N)
    // Utilizamos el número primo más cercano a N (20 -> 19)

    while (option != "0") {

        option = menu_principal(option,indice);

        // Adición de datos en el arreglo
        if (option == "1") {

            // Verificar que no supere su capacidad
            if (indice == 20) {
                cout << "Arreglo lleno" << endl << endl;

            } else {

                dato = solicitar_dato(dato,"Añadir dato");
                int hk = (dato % 19);

                // Adición directa
                if (arreglo[hk] == -1) {
                    arreglo[hk] = dato;

                    cout << "Se ha añadido: " << endl;
                    cout << "Valor -----> " << arreglo[hk] << endl;
                    cout << "Posición --> " << hk << endl << endl;

                // Adición interrumpida por colisión
                }else {
                    cout << "Colisión: " << endl;
                    cout << "Valor en el arreglo ----> " << arreglo[hk] << endl;
                    cout << "Nuevo valor ------------> " << dato << endl;
                    cout << "Posición disputada -----> " << hk << endl << endl;

                    // Evaluar método de solución
                    if (metodo == "L"){
                        hk = solve_colision->r_prueba_Lineal_adicion(arreglo,20,dato,hk,hk);
                        arreglo[hk] = dato;
                        cout << "Colisión resuelta: " << endl;
                        cout << "valor --------> " << arreglo[hk] << endl;
                        cout << "Posición -----> " << hk << endl << endl;


                    } else if (metodo  == "C") {
                        hk = solve_colision->r_prueba_Cuadratica_adicion(arreglo,20,dato,hk,hk,1);
                        arreglo[hk] = dato;
                        cout << "Colisión resuelta: " << endl;
                        cout << "valor --------> " << arreglo[hk] << endl;
                        cout << "Posición -----> " << hk << endl << endl;

                    } else if (metodo == "D") {
                        hk = solve_colision->r_Doble_direc_hash_adicion(arreglo,20,dato,hk,hk);
                        arreglo[hk] = dato;
                        cout << "Colisión resuelta: " << endl;
                        cout << "valor --------> " << arreglo[hk] << endl;
                        cout << "Posición -----> " << hk << endl << endl;

                    } else {
                        Nodo *conjunto = new Nodo();
                        solve_colision->Encadenamiento_adicion(arreglo,20,dato,hk,hk);

                    }
                }
                indice++;
            }
        
        // Busqueda de datos en el arreglo
        }else if (option == "2") {
            dato = solicitar_dato(dato,"Buscar dato");
            int hk = (dato % 19);


            // Busqueda directa
            if (arreglo[hk] == dato) {
                cout << "Dato encontrado ----> " << arreglo[hk] << endl;
                cout << "Posición del dato --> " << hk << endl << endl;
            
            // Busqueda interrumpida por colisión
            } else {

                // Evaluar método de solución
                if (metodo == "L"){
                    solve_colision->r_prueba_Lineal_busqueda(arreglo,20,dato,hk,hk);

                } else if (metodo  == "C") {
                    solve_colision->r_prueba_Cuadratica_busqueda(arreglo,20,dato,hk,hk,1);

                } else if (metodo == "D") {
                    solve_colision->r_Doble_direc_hash_busqueda(arreglo,20,dato,hk,hk);

                } else {
                    solve_colision->Encadenamiento_busqueda(arreglo,20,dato,hk,hk);

                }
            }

        // Mostrar el arreglo de tamaño 20
        }else if (option == "3") {
            cout << "Arreglo completo: " << endl;

            for (int i=0; i<20; i++) {   
                if (i == 5 || i == 10 || i == 15) {
                    cout << endl;
                }

                if (arreglo[i] != -1) {
                    cout << "[" << arreglo[i] << "]" << " ";

                } else {
                    cout << "[_]" << " ";

                }
            }
            cout << endl << endl;        
        }
    }

    return 0;
}