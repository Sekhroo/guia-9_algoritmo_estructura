#ifndef M_REASIGNACION_H
#define M_REASIGNACION_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    int dato;
    struct _Nodo *next;
} Nodo;


class m_reasignacion {

    private:
        Nodo *primer_valor = NULL;

    public:
        m_reasignacion();

        // Función para resolver colisión con reasignación lineal
        int r_prueba_Lineal_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original);
        void r_prueba_Lineal_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original);

        // Función para resolver colisión con reasignación cuadrática
        int r_prueba_Cuadratica_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original, int I);
        void r_prueba_Cuadratica_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original, int I);

        // Función para resolver colisión con reasignación de doble dirección hash
        int r_Doble_direc_hash_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original);
        void r_Doble_direc_hash_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original);

        // Función para resolver colisión con encadenamiento
        void Encadenamiento_adicion(int arreglo[], int tamano, int dato, int hk, int hk_original);
        void Encadenamiento_busqueda(int arreglo[], int tamano, int dato, int hk, int hk_original);


};
#endif